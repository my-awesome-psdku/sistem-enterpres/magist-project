-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 03, 2023 at 06:36 PM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `magist`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_barang`
--

CREATE TABLE `tbl_barang` (
  `id_barang` smallint(5) NOT NULL,
  `nama_barang` varchar(125) NOT NULL,
  `harga_beli` float NOT NULL,
  `harga_jual` float NOT NULL,
  `jumlah_stok` mediumint(7) NOT NULL,
  `satuan_barang` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_barang`
--

INSERT INTO `tbl_barang` (`id_barang`, `nama_barang`, `harga_beli`, `harga_jual`, `jumlah_stok`, `satuan_barang`) VALUES
(1, 'Tokai G7000', 14500, 15000, 9, 'Pcs'),
(2, 'Madurasa', 7000, 12000, 80, 'Karton'),
(3, 'Lilin', 4000, 10000, 11, 'Pcs'),
(4, 'Kapur Barus', 10000, 15000, 20, 'Karton'),
(5, 'Lem Joyko', 5000, 7000, 35, 'Pack'),
(6, 'Kembang Api', 7500, 10000, 65, 'Pcs');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_detail_transaksi`
--

CREATE TABLE `tbl_detail_transaksi` (
  `id_detail` bigint(25) NOT NULL,
  `id_transaksi` bigint(25) NOT NULL,
  `id_barang` smallint(5) NOT NULL,
  `jumlah` smallint(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_detail_transaksi`
--

INSERT INTO `tbl_detail_transaksi` (`id_detail`, `id_transaksi`, `id_barang`, `jumlah`) VALUES
(1, 1, 4, 5),
(2, 1, 6, 10),
(3, 1, 2, 3),
(4, 2, 1, 2),
(5, 2, 3, 12),
(6, 2, 6, 5),
(7, 3, 5, 6),
(8, 3, 2, 3),
(9, 3, 4, 8);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_transaksi`
--

CREATE TABLE `tbl_transaksi` (
  `id_transaksi` bigint(25) NOT NULL,
  `tanggal` date NOT NULL,
  `nama_pembeli` varchar(125) NOT NULL,
  `total_modal` float NOT NULL,
  `grand_total` float NOT NULL,
  `bayar` float NOT NULL,
  `kembali` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_transaksi`
--

INSERT INTO `tbl_transaksi` (`id_transaksi`, `tanggal`, `nama_pembeli`, `total_modal`, `grand_total`, `bayar`, `kembali`) VALUES
(1, '2023-01-02', 'Suden', 146000, 300000, 500000, 200000),
(2, '2023-01-02', 'Rico', 114500, 300000, 500000, 200000),
(3, '2023-01-02', 'Naya', 131000, 300000, 500000, 200000),
(8, '2023-01-03', 'ENRIQUE', 0, 660000, 1000000, 340000),
(9, '2023-01-03', 'ENRIQUE', 0, 660000, 1000000, 340000),
(10, '2023-01-03', 'ENRIQUE', 0, 660000, 1200000, 540000),
(11, '2023-01-03', 'ENRIQUE', 0, 660000, 1000000, 340000),
(12, '2023-01-03', 'ENRIQUE', 0, 660000, 1000000, 340000),
(13, '2023-01-03', 'ENRIQUE', 0, 15000, 20000, 5000),
(14, '2023-01-03', 'ENRIQUE', 0, 15000, 20000, 5000),
(15, '2023-01-03', 'ENRIQUE', 0, 15000, 20000, 5000),
(16, '2023-01-03', 'ENRIQUE', 0, 15000, 20000, 5000),
(17, '2023-01-03', 'ENRIQUE', 0, 15000, 20000, 5000),
(18, '2023-01-03', 'ENRIQUE', 0, 15000, 20000, 5000),
(19, '2023-01-03', 'ENRIQUE', 0, 15000, 20000, 5000),
(20, '2023-01-03', 'ENRIQUE', 0, 15000, 20000, 5000),
(21, '2023-01-03', 'ENRIQUE', 0, 15000, 20000, 5000),
(22, '2023-01-03', 'ENRIQUE', 0, 15000, 20000, 5000),
(23, '2023-01-03', 'ENRIQUE', 0, 15000, 20000, 5000),
(24, '2023-01-03', 'ENRIQUE', 0, 15000, 20000, 5000),
(25, '2023-01-03', 'ENRIQUE', 0, 15000, 20000, 5000),
(26, '2023-01-03', 'ENRIQUE', 0, 15000, 20000, 5000),
(27, '2023-01-03', '', 0, 15000, 20000, 5000),
(28, '2023-01-04', 'ENRIQUE', 0, 15000, 20000, 5000),
(29, '2023-01-04', 'ENRIQUE', 0, 15000, 20000, 5000);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id_user` tinyint(3) NOT NULL,
  `username` varchar(75) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id_user`, `username`, `password`) VALUES
(1, 'admin', '123');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_barang`
--
ALTER TABLE `tbl_barang`
  ADD PRIMARY KEY (`id_barang`);

--
-- Indexes for table `tbl_detail_transaksi`
--
ALTER TABLE `tbl_detail_transaksi`
  ADD PRIMARY KEY (`id_detail`),
  ADD KEY `id_barang` (`id_barang`),
  ADD KEY `id_transaksi` (`id_transaksi`);

--
-- Indexes for table `tbl_transaksi`
--
ALTER TABLE `tbl_transaksi`
  ADD PRIMARY KEY (`id_transaksi`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_barang`
--
ALTER TABLE `tbl_barang`
  MODIFY `id_barang` smallint(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_detail_transaksi`
--
ALTER TABLE `tbl_detail_transaksi`
  MODIFY `id_detail` bigint(25) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tbl_transaksi`
--
ALTER TABLE `tbl_transaksi`
  MODIFY `id_transaksi` bigint(25) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id_user` tinyint(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_detail_transaksi`
--
ALTER TABLE `tbl_detail_transaksi`
  ADD CONSTRAINT `tbl_detail_transaksi_ibfk_1` FOREIGN KEY (`id_barang`) REFERENCES `tbl_barang` (`id_barang`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_detail_transaksi_ibfk_2` FOREIGN KEY (`id_transaksi`) REFERENCES `tbl_transaksi` (`id_transaksi`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
